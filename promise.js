const express = require('express')
const app = express()
const port = process.env.PORT || 3000
const { Book } = require('./models')
app.use(express.json())
app.get('/books', 
    function(req,res){
        return(new Promise(function(resolve,reject){
            resolve('berhasil')
            if(req.body){
                Book.findAll().then(books => {
                    res.status(200).json({
                      status: true,
                      message: 'Books retrieved!',
                      data: { books }
                    })
                  })
            }
            reject(new Error('Invalid request'))
        }).then((e)=>{console.log('terim kasih')})
        .catch((err)=>{
            console.log(`Found error: ${err}`)
        }))
    }
)

app.get('/books/:id', 
    function(req,res){
        return(new Promise(function(resolve,reject){
            resolve('berhasil')
            if(req.params.id){
                Book.findByPk(req.params.id).then(book => {
                    res.status(200).json({
                      status: true,
                      message: `Books with ID ${req.params.id} retrieved!`,
                      data: book
                    })
                })
            }
            reject(new Error('Invalid request'))
        }).then((e)=>{console.log('terim kasih')})
        .catch((err)=>{
            console.log(`Found error: ${err}`)
        }))
    }
)
app.put('/books/:id', 
    function(req,res){
        return(new Promise(function(resolve,reject){
            resolve('berhasil')
            if(req.params.id){
                Book.update({
                    title: req.body.title,
                    author: req.body.author
                  }, {where: {id:req.params.id}}
                  ).then(book => {
                    res.status(201).json({
                      status: true,
                      message: 'Books updated!',
                      data: book 
                    })
                  }).catch(error => {console.log(error)})
            }
            reject(new Error('Invalid request'))
        }).then((e)=>{console.log('terim kasih')})
        .catch((err)=>{
            console.log(`Found error: ${err}`)
        }))
    }
)

app.delete('/books/:id', 
    function(req,res){
        return(new Promise(function(resolve,reject){
            resolve('berhasil')
            if(req.params.id){

                Book.destroy(
                  {where: {id:req.params.id}})
                 .then(book => {
                  res.status(201).json({
                    status: true,
                     message: `Books with id: ${req.params.id} has been deleted!`,
                     data: book 
                   })
                }).catch(error => {console.log(error)})
            }
            reject(new Error('Invalid request'))
        }).then((e)=>{console.log('terim kasih')})
        .catch((err)=>{
            console.log(`Found error: ${err}`)
        }))
    }
)

app.post('/books', 
    function(req,res){
        return(new Promise(function(resolve,reject){
            resolve('berhasil')
            if(req.body){
                Book.create({
                    title: req.body.title,
                    author: req.body.author
                  }).then(book => {
                    res.status(201).json({
                      status: true,
                      message: 'Books created!',
                      data: book 
                    })
                })
            }
            reject(new Error('Invalid request'))
        }).then((e)=>{console.log('terim kasih')})
        .catch((err)=>{
            console.log(`Found error: ${err}`)
        }))
    }
)
app.listen(port, ()=> console.log(`Listen to port: ${port} on localhost`))