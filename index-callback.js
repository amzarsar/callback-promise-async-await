const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
// const { Book, User } = require('./models')
const { Book } = require('./models')
app.use(express.json())
app.get('/books', (req, res) => {
  Book.findAll().then(books => {
    res.status(200).json({
      status: true,
      message: 'Books retrieved!',
      data: { books }
    })
  })
})
app.post('/books', (req, res) => {
  Book.create({
    title: req.body.title,
    author: req.body.author
  }).then(book => {
    res.status(201).json({
      status: true,
      message: 'Books created!',
      data: book 
    })
  })
})

app.put('/books/:id', (req, res) => {
  console.log(`Updating data with id: ${req.params.id}`)
  Book.update({
    title: req.body.title,
    author: req.body.author
  }, {where: {id:req.params.id}}
  ).then(book => {
    res.status(201).json({
      status: true,
      message: 'Books updated!',
      data: book 
    })
  }).catch(error => {console.log(error)})
})

app.delete('/books/:id', (req, res) => {
  console.log(`Deleting data with id: ${req.params.id}`)
  Book.findByPk(req.params.id).then(resultToDelete=>{resultToDelete.destroy(req.params.id)})
  Book.destroy(
    {where: {id:req.params.id}})
   .then(book => {
    res.status(201).json({
      status: true,
       message: `Books with id: ${req.params.id} has been deleted!`,
       data: book 
     })
  }).catch(error => {console.log(error)})
})

app.get('/books/:id', (req, res) => {
  Book.findByPk(req.params.id).then(book => {
    res.status(200).json({
      status: true,
      message: `Books with ID ${req.params.id} retrieved!`,
      data: book
    })
  })
})

app.listen(port, () => console.log(`Listening on port ${port}!`))